const arregloRandom = [];
for (let i = 0; i < 20; i++) {

  arregloRandom.push(Math.floor(Math.random() * (100 - 1) + 1));

}

let mediopro = document.getElementById('mediopro')

function imprimirPromedio(array, elemento)  {
  let content = "";
  let prom = 0;
  array.forEach(item => {
      content += "<p>" + item + "</p>";
      prom = prom + item;
  });
  prom = prom / array.length;
  mediopro.innerHTML = "Promedio: " + prom;
  elemento.innerHTML = content;
}

imprimirPromedio(arregloRandom, document.getElementById('arreglo'));

function imprimirPares(){

  let pares = document.getElementById('pares')
  let numPares = 0

    for(let x=0; x < arregloRandom.length; ++x){
        if((arregloRandom[x] %2) == 0){
            numPares += 1
        }
    }
       
    pares.innerHTML = "Pares: " + numPares; 

}

imprimirPares();

function mayormenor(){

  let mayoramenor = document.getElementById('mayoramenor')
   let ordenar = arregloRandom.sort((a,b)=>{
      return b-a;
   })

   mayoramenor.innerHTML = "Mayor a Menor: " + ordenar; 

}

mayormenor();
